from uni_app.university_app import app
from flask_login import LoginManager
from uni_app.university_app.models.user_models import User


# this block ensures the functionality of login.
# YOU WILL NEED flask_login to make it work properly:
# pip install Flask-Login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@login_manager.user_loader
def load_user(user_id):
	try:
		return User.get_id(user_id)
	except:
		return None


if __name__ == '__main__':
	app.run(host='localhost', port=8080, debug=True)
