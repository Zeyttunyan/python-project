from uni_app.university_app import get_db
import pandas as pd
import plotly.graph_objects as go
import plotly.io as pio
import chart_studio.plotly as py
import chart_studio
import sqlite3
import googlemaps
import requests, json

class Insights:
    def __init__(self, ):
        self.conn = get_db()
        self.curs = get_db('row').cursor()
        self.dct = get_db('dict').cursor()
        self.colorscale = ["#f7fbff", "#ebf3fb", "#deebf7", "#d2e3f3", "#c6dbef", "#b3d2e9", "#9ecae1",
                      "#85bcdb", "#6baed6", "#57a0ce", "#4292c6", "#3082be", "#2171b5", "#1361a9",
                      "#08519c", "#0b4083", "#08306b"]


    def diversity_tuition(self):
        self.curs.execute('SELECT american_indian_or_alaska_native as race_1, '
                          'asian as race_2, black_or_african_american as race_3, '
                          'hispanic_or_latino as race_4, '
                          'native_hawaiian_other_pacific_islander as race_5, '
                          'white as race_6, two_and_more_race as race_7, '
                          'unknown_race as race_8, non_resident_alien as race_9, num_students, tuition '
                          'FROM Universities WHERE num_students > 1')
        data = {}
        all = self.curs.fetchall()
        for row in all:
            students_num = row[-2]
            race_number = 0
            for race in row[:-2]:
                n = race * students_num / 100
                race_number += n * (n - 1)
            N = students_num * (students_num - 1)
            diversity = 1 - (race_number / N)
            data[diversity] = row[-1]

        return data
    def size_tuition(self):
      self.df = pd.read_sql_query('SELECT name, tuition, num_students, position FROM Universities JOIN UniversityRankings ON UniversityRankings.uni_id=Universities.id', 
                                  self.conn)
      return self.df

    @staticmethod
    def get_mapping():
        mapping = {'Caucasians': 'white',
               'Native Islanders':'native_hawaiian_other_pacific_islander',
               'Native Americans':'american_indian_or_alaska_native',
               'Mixed Race': 'two_and_more_race',
               'African Americans': 'black_or_african_american',
               'Asians':'asian',
               'Hispanics':'hispanic_or_latino',
               'Internationals': 'non_resident_alien',
               'Unknown': 'unknown_race'}
        return mapping

    def races_percents(self, race):
        con = get_db()
        query = ('SELECT '
                 'Universities.num_students, '
                 'Universities.unknown_race, '
                 'Universities.native_hawaiian_other_pacific_islander, '
                 'Universities.white, '
                 'Universities.two_and_more_race, '
                 'Universities.black_or_african_american, '
                 'Universities.asian, '
                 '"Universities".hispanic_or_latino, '
                 'Universities.non_resident_alien, '
                 'Universities.american_indian_or_alaska_native, '
                 'States.name '
                 'FROM Universities '
                 'LEFT JOIN Cities ON Universities.city_id=Cities.id '
                 'LEFT JOIN States ON Cities.state_id=States.id')
        original_table = pd.read_sql_query(query, con)

        # counting absolute numbers from percents given
        new_table = pd.DataFrame()
        new_table['state_name'] = original_table['name']
        new_table['number_of_students'] = original_table['num_students']

        for col_name, col_contents in original_table.iloc[:, 1:-1].iteritems():
            new_table[col_name] = col_name
            new_table[col_name] = (original_table.iloc[:, 0] * col_contents.values) / 100

        # consolidating the data by state
        new_table = new_table.groupby(['state_name']).sum().reset_index()

        # converting everythig back to percents
        last_table = pd.DataFrame()
        last_table['state_name'] = new_table['state_name']
        last_table['number_of_students'] = new_table['number_of_students']

        for col_name, col_contents in new_table.iloc[:, 2:].iteritems():
            last_table[col_name] = col_name
            last_table[col_name] = (col_contents.values / last_table.iloc[:, 1]) * 100

        # plotting the data

        # setting the formal name of race for nice display in the graph
        races_mapping = self.get_mapping()
        for key, value in races_mapping.items():
            if value == race:
                race_name = key

        info = last_table

        fig = go.Figure(data=go.Choropleth(locations=info['state_name'], text=info['state_name'], colorscale=self.colorscale,
                                           locationmode='USA-states', z=info[race], colorbar_title="Percents"))
        fig.update_geos(showlakes=True, showrivers=True, scope='usa')
        fig.update_layout(title_text=f'Relative Share of {race_name} in Different States Of the USA')
        return fig, race_name


    def chart_deploy_to_plotly(self, fig, filename):
            # deploying to chart studio to get a link for embedding
            username = 'hharut'
            api_key = 'Znoa0lw8kprWoLFFbjqi'
            chart_studio.tools.set_credentials_file(username=username, api_key=api_key)
            embed_link = py.plot(fig, filename=filename, auto_open=True)
            return embed_link

    def maps_by_locations(self, category_name):
        self.curs.execute('SELECT  Universities.name '
                               # 'Universities.street,'
                               # 'Universities.postal,Cities.name,'
                               # 'Universities.name as name '
                               'FROM Categories '
                               'LEFT JOIN CategoryRankings ON'
                               ' Categories.id = CategoryRankings.category_id '
                               'LEFT JOIN Universities '
                               'ON  Universities.id = CategoryRankings.uni_id '
                               'LEFT JOIN Cities ON Cities.id = Universities.city_id'
                               ' WHERE Categories.name = "{}"'
                               'ORDER BY CategoryRankings.position LIMIT 10 '.format(category_name)

                          )
        university = self.curs.fetchall()
        # enter your api key here
        api_key = 'AIzaSyBr4Ghh5FLjv-6rZvWPoCUfedH7nYt2Hng'
        # url variable store url
        url = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
        map_list = []
        count = 0
          # get method of requests module
          # return response object
        for query in university:

            r = requests.get(url + 'query=' + query[0] +
                      '&key=' + api_key)

            # json method of response object convert
            # json format data into python format data
            x = r.json()
            print(x)
            # now x contains list of nested dictionaries
            # we know dictionary contain key value pair
            # store the value of result key in variable y
            y = x['results']


            map_list.append([])
            map_list[count].append(y[0]['name'])
            map_list[count].append(y[0]["formatted_address"])
            map_list[count].append(y[0]["geometry"]['location']['lat'])
            map_list[count].append(y[0]["geometry"]['location']['lng'])
            map_list[count].append(count)
            count += 1
            

        return map_list
    def average_prices(self):
        con = get_db(type='dict')
        query=('SELECT avg("Universities".tuition), States.name '
                               'FROM "Universities" '
                               'LEFT JOIN "Cities" ON "Universities".city_id="Cities".id '
                               'LEFT JOIN "States" on "Cities".state_id="States".id '
                               'GROUP BY "States".name')

        average_price = pd.read_sql_query(query, con)

        print(average_price.head())
        fig = go.Figure(data=go.Choropleth(locations=average_price['name'], text=average_price['name'], colorscale=self.colorscale,
                                           locationmode='USA-states', z=average_price['avg("Universities".tuition)'], colorbar_title="USD"))
        fig.update_geos(showlakes=True, showrivers=True, scope='usa')

        fig.update_layout(title_text='Average Price of University in Different States Of the USA')

        return self.chart_deploy_to_plotly(fig, "average_prices" )

    def gender_rank(self):
        all =self.dct.execute('SELECT  un.male as male, un.female as female,un.name as name,'
                              ' rk.position as rank' \
                              ' FROM  Universities un JOIN UniversityRankings rk' \
                              ' ON un.id=rk.uni_id  ORDER by un.female DESC ')
        all = self.dct.fetchall()
        return all

    def unis_category(self):
        cur = self.dct.execute('Select uni.name as univ, rk.position , ct.name as categ ,COUNT(1) as cnt '
                               'FROM Universities as uni '
                               'JOIN CategoryRankings as rk ON rk.uni_id=uni.id '
                               'JOIN Categories as ct ON ct.id = rk.category_id '
                               'WHERE rk.position < 5 '
                               'Group BY uni.id'
                               # ', ct.id '
                               )
        return cur
