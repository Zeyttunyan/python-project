from werkzeug.security import generate_password_hash,check_password_hash
from flask_login import UserMixin
from uni_app.university_app import get_db


class User(UserMixin):

    def __init__(self, first_name, last_name, password, email):
        self.first_name = first_name
        self.last_name = last_name
        self.password = password
        self.pass_hash = generate_password_hash(password)
        self.id = email
        self.conn = get_db(type='dict')
        self.cursor = self.conn.cursor()

    @staticmethod
    def get_user(email):
        cursor = get_db(type='dict').cursor()
        user = cursor.execute('SELECT * FROM User WHERE email=?', (email,)).fetchone()
        if user:
            new_user = User(user['first_name'], user['last_name'], user['pass_hash'], user['email'])
            return new_user
        return False

    def add_user(self):
        pass_hash = generate_password_hash(self.password)
        self.cursor.execute("INSERT INTO User (first_name, last_name, pass_hash, email) "
                            "VALUES (?, ?, ?, ?)", (self.first_name, self.last_name, pass_hash, self.id))
        self.conn.commit()

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def get_id(self):
        return self.id

    def __repr__(self):
        return f"Email: {self.id}, Name: {self.first_name}, {self.last_name}, {self.pass_hash}"



    def add_favourite(self, uni_id):
        uid = self.cursor.execute("SELECT id FROM User WHERE email=?", (self.id,)).fetchone()['id']

        if not self.cursor.execute('SELECT uni_id FROM User_Universities WHERE uni_id = ?', (uni_id,)).fetchone():
            self.cursor.execute('INSERT INTO User_Universities (user_id, uni_id) VALUES (?, ?)', (uid, uni_id))
            self.conn.commit()
            return True
        return False

    def get_favourites(self):
        uid = self.cursor.execute("SELECT id FROM User WHERE email=?", (self.id,)).fetchone()['id']
        self.cursor.execute("SELECT Universities.id, Universities.name "
                            "FROM Universities "
                            "LEFT JOIN User_Universities "
                            "ON uni_id= Universities.id "
                            "WHERE User_Universities.user_id = ?", (uid,))

        favourites = self.cursor.fetchall()
        return favourites


    def delete(self, uni_id):
        uid = self.cursor.execute("SELECT id FROM User WHERE email=?", (self.id,)).fetchone()['id']
        self.cursor.execute("DELETE FROM User_Universities "
                            "WHERE uni_id= ? "
                            "AND user_id = ? ", (uni_id, uid))
        self.conn.commit()
