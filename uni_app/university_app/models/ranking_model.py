from uni_app.university_app import get_db


class Ranking:
    def __init__(self, ):
        self.curs = get_db('row').cursor()

    def get_all_ranking_names(self):
        self.curs.execute('SELECT Categories.name as category_name '
                          'FROM CategoryRankings LEFT JOIN Categories ON '
                          'CategoryRankings.category_id = Categories.id '
                          'ORDER BY Categories.name ASC ')
        rankings = []
        for row in self.curs:
            rankings.append(row[0])
        rankings = list(dict.fromkeys(rankings))
        return rankings

    def get_id(self, category_name):
        self.curs.execute('SELECT Categories.id as category_id '
                          'FROM CategoryRankings LEFT JOIN Categories ON '
                          'CategoryRankings.category_id = Categories.id '
                          'WHERE Categories.name = ?', (category_name,))

        return self.curs.fetchone()['category_id']

    def get_count(self, category_id):
        self.curs.execute('SELECT count(*) as cnt '
                          'FROM CategoryRankings LEFT JOIN Universities ON '
                          'CategoryRankings.uni_id = Universities.id '
                          'WHERE CategoryRankings.category_id = (?)',
                          (category_id,))

        return self.curs.fetchone()['cnt']

    def get_uni_list(self, limit, offset, category_id):
        self.curs.execute('SELECT CategoryRankings.position as position, '
                          'CategoryRankings.uni_id as uni_id, '
                          'Universities.name as uni_name '
                          'FROM CategoryRankings LEFT JOIN Universities ON '
                          'CategoryRankings.uni_id = Universities.id '
                          'WHERE CategoryRankings.category_id = (?) '
                          'ORDER BY CategoryRankings.position '
                          f'LIMIT {limit} OFFSET {offset}', (category_id,))

        return self.curs

    def get_count_global(self):
        self.curs.execute('SELECT count(*) as cnt FROM UniversityRankings')

        return self.curs.fetchone()['cnt']

    def get_uni_list_global(self, limit, offset):
        self.curs.execute('SELECT UniversityRankings.position as position, '
                          'UniversityRankings.uni_id as uni_id, '
                          'Universities.name as uni_name '
                          'FROM UniversityRankings LEFT JOIN Universities ON '
                          'UniversityRankings.uni_id = Universities.id '
                          'ORDER BY UniversityRankings.position '
                          f'LIMIT {limit} OFFSET {offset}')

        return self.curs
