from flask import Flask, g

import sqlite3
import os


app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = 'mysecretkey'        # this will have to go to a config.py file later
DATABASE = os.path.join(app.root_path, 'universities.db')
app.config['DATABASE'] = DATABASE


def get_db(type=None):
    '''
    this function returns the connection to the db
    if you pass type='dict', the select statement will return dicts.
    if you pass 'row', the select statement will return Row objects
    if you pass nothing, it will return tuples
    TO USE THE CONNECTION AND THE CURSOR:
    import get_db() from this file and add
    cursor = get_db().cursor() in your file
    '''
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    if type == 'dict':
        db.row_factory = make_dicts
    elif type == 'row':
        db.row_factory = sqlite3.Row
    return db


# this function ensures the functionality of get_db()
def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))


# with app.app_context():
#     app.db = get_db()

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()



from uni_app.university_app.views.core import core
from uni_app.university_app.views.data_insights import data_insights
from uni_app.university_app.views.maps import uni_maps
from uni_app.university_app.views.rankings import uni_rankings
from uni_app.university_app.views.search import uni_search
from uni_app.university_app.views.users import user_management


app.register_blueprint(core)
app.register_blueprint(data_insights)
app.register_blueprint(uni_maps)
app.register_blueprint(uni_rankings)
app.register_blueprint(uni_search)
app.register_blueprint(user_management)

# should we need to fix dynamic routing
# here is what we do
# app.register_blueprint(core, url_prefix='</core_url_slug>' )

