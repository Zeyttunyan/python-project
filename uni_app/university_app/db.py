import sqlite3
import os
from uni_app.university_app import app

DATABASE = app.config['DATABASE']


class SQLiteDB:

    def __init__(self, datfile: str):
        self.conn = sqlite3.connect(datfile)
        #self.set_schema()

    def run(self, query: str, args: tuple = tuple()) -> object:
        cursor = self.conn.cursor()
        try:
            cursor.execute(query, args)
            self.conn.commit()
            return cursor
        except sqlite3.DatabaseError as dbex:
            self.conn.rollback()
            raise dbex

    def set_schema(self):

        CREATE_SQL = (
            """
            PRAGMA foreign_keys = ON;
            CREATE TABLE IF NOT EXISTS Sources (
              id INTEGER PRIMARY KEY,
              name text UNIQUE NOT NULL,
              upd_time  DATETIME  DEFAULT CURRENT_TIMESTAMP
            );

            CREATE TABLE IF NOT EXISTS States (
              id INTEGER PRIMARY KEY,
              name text  Unique NOT NULL,
              source_id INTEGER,
              upd_time  DATETIME  DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );

            CREATE TABLE IF NOT EXISTS Cities (
              id INTEGER PRIMARY KEY,
              name text NOT NULL,
              state_id INTEGER,
              source_id INTEGER,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (state_id) REFERENCES States(id)  ON DELETE CASCADE,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );


            CREATE TABLE IF NOT EXISTS Universities (
              id INTEGER PRIMARY KEY,
              name text NOT NULL,
              postal INTEGER UNIQUE,
              street text,
              city_id INTEGER,
              num_programs INTEGER,
              num_students INTEGER,
              tuition INTEGER,
              admission fee INTEGER,
              white DECIMAL,
              black DECIMAL,
              male DECIMAL,
              female DECIMAL,
              source_id INTEGER,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (city_id)  REFERENCES Cities(id) ON DELETE SET NULL,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );

            CREATE TABLE IF NOT EXISTS Programs (
              id INTEGER PRIMARY KEY,
              uni_id INTEGER,
              name text NOT NULL,
              category_id INTEGER,
              source_id INTEGER,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (uni_id) REFERENCES Universities(id)  ON DELETE CASCADE,
              FOREIGN KEY (category_id) REFERENCES Categories(id)  ON DELETE CASCADE,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );

            CREATE TABLE IF NOT EXISTS Categories (
              id INTEGER PRIMARY KEY,
              name text NOT NULL,
              source_id INTEGER,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );

            CREATE TABLE IF NOT EXISTS UniversityRankings (
              id INTEGER PRIMARY KEY,
              uni_id INTEGER,
              position INTEGER,
              source_id INTEGER,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (uni_id) REFERENCES Universities(id)  ON DELETE CASCADE,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );

            CREATE TABLE IF NOT EXISTS CategoryRankings  (
              id INTEGER PRIMARY KEY,
              uni_id INTEGER,
              category_id INTEGER,
              position INTEGER,
              source_id INTEGER,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (category_id) REFERENCES Categories(id)  ON DELETE CASCADE,
              FOREIGN KEY (source_id) REFERENCES Sources(id)  ON DELETE SET NULL
            );

            CREATE TABLE IF NOT EXISTS User_Universities (
              id INTEGER PRIMARY KEY,
              user_id INTEGER,
              uni_id INTEGER,
              upd_time  DATETIME  DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (uni_id) REFERENCES Universities(id)  ON DELETE CASCADE
            );


            CREATE TABLE IF NOT EXISTS User (
              id INTEGER PRIMARY KEY,
              first_name text,
              last_name text,
              pass_hash text,
              email text UNIQUE NOT NULL ,
              upd_time  DATETIME DEFAULT CURRENT_TIMESTAMP
            );

            """
        )

        cursor = self.conn.cursor()
        try:
            cursor.executescript(CREATE_SQL)
            self.conn.commit()
            return cursor
        except sqlite3.DatabaseError as dbex:
            self.conn.rollback()
            raise dbex

    def __del__(self):
        self.conn.close()


if __name__ == '__main__':
    db = SQLiteDB(DATABASE)
    db.set_schema()
