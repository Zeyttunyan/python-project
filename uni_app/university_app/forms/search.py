from wtforms import IntegerField, SelectField, SubmitField, ValidationError
from flask_wtf import FlaskForm



class IntegerValidationError(ValidationError):
    pass


def Integer(form, field):
    if not isinstance(field.data, int):#and field is not None:
        raise IntegerValidationError


class AdvancedSearch(FlaskForm):
    category = SelectField('Category')
    university = SelectField('University')
    locat = SelectField('Cities')
    tut_great = IntegerField('Min Tuition', [Integer])
    tut_less = IntegerField('Max Tuition', [Integer])
    enroll_great = IntegerField('Min Enrollment', [Integer])
    enroll_less = IntegerField('Max Enrollment', [Integer])
