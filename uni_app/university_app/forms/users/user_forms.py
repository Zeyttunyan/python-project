from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired,Email,EqualTo
# from wtforms import ValidationError


class LoginForm(FlaskForm):

    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')

class RegistrationForm(FlaskForm):

    email = StringField('Email', validators=[DataRequired(), Email()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('pass_confirm', message='Passwords Must Match!')])
    pass_confirm = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Register!')

    # def validate_email(self, email):
    #     cursor = SQLiteDB('universities.db')  # will have from app context
    #     table = SQLTable(cursor, 'User')
    #     user = cursor.execute(f"SELECT * FROM User WHERE email={email}").fetchone()
    #     if user:
    #         raise ValidationError('Your email has been registered already!')





# class UpdateUserForm(FlaskForm):
#     email = StringField('Email', validators=[DataRequired(),Email()])
#     username = StringField('Username', validators=[DataRequired()])
#     picture = FileField('Update Profile Picture', validators=[FileAllowed(['jpg', 'png'])])
#     submit = SubmitField('Update')
#
#     def validate_email(self, field):
#         # Check if not None for that user email!
#         # if CHECK IF ALREDY THERE IN DB:
#             raise ValidationError('Your email has been registered already!')
#
#     def validate_username(self, field):
#         # Check if not None for that username!
#         # if User.query.filter_by(username=field.data).first():
#             raise ValidationError('Sorry, that username is taken!')

