import flask
from flask import render_template, Blueprint, flash, redirect, url_for, session, abort
from flask_login import login_user, login_required, logout_user
from uni_app.university_app.forms.users.user_forms import RegistrationForm, LoginForm
from uni_app.university_app.models.user_models import User



user_management = Blueprint('user_management', __name__, template_folder='../templates/users')


@user_management.route('/register', methods=['GET', "POST"])
def register():

    form = RegistrationForm()

    if form.validate_on_submit():
        user = User(form.first_name.data,
                     form.last_name.data,
                     form.password.data,
                     form.email.data)

        user.add_user()
        flash("Registration Good")
        return redirect(url_for('user_management.login'))
    return render_template('register.html', form=form)


@user_management.route('/login', methods=['GET', "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        email = form.email.data

        user = User.get_user(email)
        if user:
            if user.check_password(form.password.data):

                login_user(user, force=True)
                session['user'] = user.id

                next = flask.request.args.get('next')
                return flask.redirect(next or flask.url_for('core.simplesearch'))

            else:
                flash('Login failed, try again')
                return render_template('register.html')
    return render_template('login.html', form=form)


@user_management.route('/logout')
# @login_required
def logout():
    session['user'] = False
    logout_user()
    return redirect(url_for('core.simplesearch'))


@user_management.route('/myaccount')
def myaccount():
    if not session['user']:
        abort(403)
        return redirect(url_for('user_management.login'))
    else:
        user_id = session['user']
        user = User.get_user(user_id)
        favourites = user.get_favourites()

        # show name, last, and favorite unis
        pass
    return render_template('my_account.html', user=user, favourites=favourites)


@user_management.route('/favourites/<uni_id>')
def favourites(uni_id):
    if not session['user']:
        return redirect(url_for('user_management.login'))
    else:
        user_id = session['user']
        user = User.get_user(user_id)
        user.add_favourite(uni_id)
        flash('Added to your favourites')
    return redirect(f'../unis/{uni_id}')

@user_management.route('/delete/<uni_id>')
def delete(uni_id):
    user_id = session['user']
    user = User.get_user(user_id)
    user.delete(uni_id)

    return redirect(url_for('user_management.myaccount'))





