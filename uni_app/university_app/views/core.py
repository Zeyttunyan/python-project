from flask import request, make_response
from uni_app.university_app.tb_ini import SQLTable
from uni_app.university_app.sql_queries import SQLMultiTable
import math
from uni_app.university_app import app
from flask import render_template, Blueprint
from uni_app.university_app.models.universities_model import University
import random
from uni_app.university_app.models.ranking_model import Ranking

core = Blueprint('core', __name__, template_folder='../templates/core')
DATABASE = app.config['DATABASE']


def topunis():
    uniobj = Ranking()
    top10 = uniobj.get_uni_list_global(10,0)
    return top10

@core.route('/',methods=['GET', 'POST'])
def simplesearch():
    top10 = topunis()
    if request.method == 'POST':
        page = int(request.form.get('page', 1))
        items_per_page = 10
        name = request.form.get('name', '')
        unobj = SQLTable('Universities')
        if name:
            queryobj = SQLMultiTable()
            cur = queryobj.simple_join_search(name, items_per_page,items_per_page*page)
        page_count = math.ceil(queryobj.simple_join_search_count(name) / items_per_page)
        resp = make_response(
            render_template('simple_search.html',cur = cur, name=name, page = page))
        return resp
    cat = SQLTable('Universities')
    cur = cat.select_distinct('name')
    return render_template('index.html', top10 = top10, cur=cur)


@core.route('/ourteam')
def ourteam():
    return render_template('ourteam.html')


@core.route('/unis/<query>')
def uni_templates(query):
    uni = University(query)
    plot = uni.make_plot()
    programs = uni.get_programs()
    image_num = random.randint(1, 48)
    image = f"/static/images/uni_pictures/{image_num}.png"
    return render_template('university_template.html', uni=uni, plot=plot, programs=programs, image=image)

if __name__ == '__main__':
    simplesearch()