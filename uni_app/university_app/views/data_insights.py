from flask import render_template, Blueprint
from uni_app.university_app.models.insights_model import Insights
import matplotlib.pyplot as plt
import os
from numpy.polynomial.polynomial import polyfit
import numpy as np
import pandas as pd
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import matplotlib
from io import BytesIO
import base64

data_insights = Blueprint('insights', __name__, template_folder='../templates/data_insights')


def reg_tuition():
    plt.clf()
    df = Insights().size_tuition()
    X = df['tuition'].values.reshape(-1,1)
    y = df['position'].values.reshape(-1,1)
    model = LinearRegression()  
    model.fit(X, y)
    coef = round(model.coef_[0][0], 4)
    r_sq = round(model.score(X, y),3)
    #predict part on testing set
    y_pred =  model.predict(X)

    #plt.style.use('fivethirtyeight')
    plt.scatter(X, y)
    plt.plot(X, y_pred, linewidth=2, label = 'Regression line')
    #plt.style.use('seaborn-colorblind')
    plt.title('Rank vs Tuition')  
    plt.xlabel('Tuition')  
    plt.ylabel('Rank') 
    plt.plot([], [], ' ', label=f'R-squared = {r_sq}\nSlope = {coef}')
    plt.legend()
    path = os.path.join('static', 'images', 'insights_graphs', 'rank_tuition.png')
    # fig.write_image(os.path.join('uni_app', 'university_app', path))
    plt.savefig(os.path.join('uni_app', 'university_app', path))
    return path

def reg_size():
    plt.clf()
    df = Insights().size_tuition()
    X = df['num_students'].values.reshape(-1,1)
    y = df['position'].values.reshape(-1,1)
    model = LinearRegression()  
    model.fit(X, y)
    coef = round(model.coef_[0][0], 4)
    r_sq = round(model.score(X, y),3)
    #predict part on testing set
    y_pred =  model.predict(X)

    plt.scatter(X, y)
    plt.plot(X, y_pred, linewidth=2, label = 'Regression line')
    #plt.style.use('seaborn-colorblind')
    plt.title('Rank vs Number of Students')  
    plt.xlabel('Number of Students')  
    plt.ylabel('Rank') 
    plt.plot([], [], ' ', label=f'R-squared = {r_sq}\nSlope = {coef}')
    plt.legend()
    path = os.path.join('static', 'images', 'insights_graphs', 'rank_size.png')
    # fig.write_image(os.path.join('uni_app', 'university_app', path))
    plt.savefig(os.path.join('uni_app', 'university_app', path))
    return path


@data_insights.route('/insights')  # please don't change this route
def insights():
    return render_template('data_insights.html')


@data_insights.route('/diversity')
def diversity_tuition():
    data = Insights().diversity_tuition()
    matplotlib.use('Agg')
    plt.title('Correlation between diversity index and tuition')
    plt.xlabel('Diversity')
    plt.ylabel('Tuition')
    x = np.array(list(data.keys()))
    y = np.array(list(data.values()))
    b, m = polyfit(x, y, 1)
    r = np.corrcoef(x, y)[0][1]
    plt.plot(x, y, '.', color='#003F72', label="data")
    plt.plot(x, b + m * x, '-', color="c", label="regression line")
    plt.legend(loc=2)
    plt.grid(color="g", linestyle='-', linewidth=0.2)
    tempfile = BytesIO()
    plt.savefig(tempfile, format='png')
    plt.close()
    encoded = base64.b64encode(tempfile.getvalue()).decode('utf-8')
    return render_template("diversity_tuition.html", image=encoded, r=r)


@data_insights.route('/gender_rank')
def gender_rank():
    data = Insights().gender_rank()
    df = pd.DataFrame(data)
    X = df['female'].values[:, np.newaxis]
    # print(X)
    y = df['rank'].values
    fig = Figure()
    canvas = FigureCanvas(fig)
    axis = fig.add_subplot(1, 1, 1)
    axis.set_xlabel('female')
    axis.set_ylabel('university ranking')
    axis.set_title('Correlation between female enrollment and university rank')
    model = LinearRegression()
    fit = model.fit(X, y)
    # The coefficients
    coef = fit.coef_
    interc = fit.intercept_
    axis.scatter(X, y, color='g')
    y_pred= model.predict(X)
    axis.plot(X, y_pred, color='r')
    r_sq = r2_score(y, y_pred)
    path = os.path.join('static', 'images', 'insights_graphs', 'women_stat.png')
    canvas.print_figure(os.path.join('uni_app', 'university_app', path))
    # fig.savefig(os.path.join('uni_app', 'university_app', path))
    return render_template("gender_rank.html", path=path, coef=coef[0], interc=interc, r_sq= r_sq)


@data_insights.route('/size_tuition_rank')
def size_rank_tuition():
    path1 = reg_tuition()
    path2 = reg_size()
    return render_template("data_insights.html", path1 = path1, path2 = path2)

@data_insights.route('/university_categories')
def unis_top():
    data = Insights().unis_category()
    df = pd.DataFrame(data)
    plts = df.plot.bar(x='univ', y='cnt')
    fig = plts.get_figure()
    fig.tight_layout()
    path_all = os.path.join('uni_app', 'university_app', 'static', 'images', 'insights_graphs', 'unis_top.png')
    fig.savefig(path_all)
    path = os.path.join('static', 'images', 'insights_graphs', 'unis_top.png')
    return render_template("unis_category.html", path=path)
