from flask import render_template, request, redirect, Blueprint, url_for
from uni_app.university_app.models.ranking_model import Ranking
import math

uni_rankings = Blueprint('rankings', __name__, template_folder='../templates/rankings')


@uni_rankings.route('/rankings')
def ranking():
    rank = Ranking()
    rankings = rank.get_all_ranking_names()
    query = request.args.get('ranking_name')
    if query in rankings:
        query = query.replace('/', '. ')
        return redirect(f'/rankings/{query}')
    rankings_len = len(rankings)
    image_count = 6
    random_list = \
        list(range(1, image_count + 1)) * math.ceil(rankings_len / image_count)
    return render_template('ranking_list.html', rankings=rankings,
                           len=rankings_len, random_list=random_list)


@uni_rankings.route('/rankings/<query>')
def universities(query):
    rank = Ranking()
    query = query.replace('. ', '/')
    requested_page = int(request.args.get('page', 1))
    if query == "global":
        cnt = rank.get_count_global()
        page_count, limit, offset = pagination(cnt, requested_page)
        cursor = rank.get_uni_list_global(limit, offset)
    elif query in rank.get_all_ranking_names():
        category_id = rank.get_id(query)
        cnt = rank.get_count(category_id)
        page_count, limit, offset = pagination(cnt, requested_page)
        cursor = rank.get_uni_list(limit, offset, category_id)
    else:
        cursor = ""
        page_count = 0

    query_uni_id = request.args.get('uni_id')
    if query_uni_id:
        return redirect(url_for("core.uni_templates", query=query_uni_id))
    return render_template("uni_rankings.html",
                           uni_list=cursor, page_count=page_count,
                           page=requested_page, category_name=query)


def pagination(cnt, requested_page):
    items_per_page = 20
    page_count = math.ceil(cnt / items_per_page)
    limit = items_per_page
    offset = (requested_page - 1) * items_per_page
    return page_count, limit, offset
