from flask import render_template, Blueprint, request
from uni_app.university_app.models.insights_model import Insights
import googlemaps
import chart_studio.tools as tls
import json

from uni_app.university_app import get_db

uni_maps = Blueprint('maps', __name__, template_folder='../templates/maps')


@uni_maps.route('/maps', methods=['GET'])
def maps():

    query = request.args.get('category')

    return render_template('maps.html')


@uni_maps.route('/maps/<category_name>', methods=['GET'])
def maps_by_locations(category_name):

    query = Insights()
    category_name = category_name.replace('%', ' ')
    category_name.replace('$', '/')
    map_list = query.maps_by_locations(category_name)
    json_map = json.dumps(map_list)

    print(json_map)

    return render_template('maps_result.html', map=json_map)





@uni_maps.route('/races_by_state')
def races_by_state():
    races_mapping = Insights.get_mapping()
    return render_template('races_percent.html', races_mapping=races_mapping)

@uni_maps.route('/race_choice/<race>')
def race_choice(race):
    races_mapping = Insights.get_mapping()

    # plotly_graph_mapping = {23: 'white',
    #                         25: 'native_hawaiian_other_pacific_islander',
    #                         27: 'american_indian_or_alaska_native',
    #                         30: 'two_and_more_race',
    #                         32: 'black_or_african_american',
    #                         34:'asians',
    #                         37: 'hispanic_or_latino',
    #                         39: 'non_resident_alien',
    #                         41: 'unknown_race'
    #                         }


    # This is for creating the plots in case they go missing
    # race_graph = Insights()
    # fig, race_name =  race_graph.races_percents(race)
    # embed_link = race_graph.chart_deploy_to_plotly(fig, race_name)

    # this will return the html tag to embed
    # print(tls.get_embed(embed_link))

    return render_template('races_percent.html', races_mapping=races_mapping, race=race)

@uni_maps.route('/states_prices')
def states_prices():

    # This is for creating the plots in case they go missing
    # insight = Insights()
    # insight.average_prices()

    return render_template('average_prices.html')
