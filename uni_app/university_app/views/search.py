from flask import render_template, Blueprint,request,make_response, flash, redirect

# from tb_ini import SQLiteDB
from uni_app.university_app.tb_ini  import SQLTable
from uni_app.university_app.tb_ini  import SQLiteDB
from uni_app.university_app.sql_queries  import SQLMultiTable
import math
from uni_app.university_app import app
from uni_app.university_app.forms.search import AdvancedSearch, IntegerValidationError

uni_search = Blueprint('search', __name__, template_folder='../templates/search')


@uni_search.route('/search', methods=['GET', 'POST'])
def search():
    category = SQLTable('Categories')
    cur_category = category.select_all()
    category_list = [(i[1], i[1]) for i in cur_category]

    university = SQLTable('Universities')
    cur_university = university.select_all()
    university_list = [(i[1], i[1]) for i in cur_university]

    city = SQLTable('Cities')
    cur_city = city.select_all()
    city_list = [(i[1], i[1]) for i in cur_city]

    form = AdvancedSearch(request.form)
    form.category.choices = [('', '-- select an option --')] + category_list
    form.university.choices = [('', '-- select an option --')] + university_list
    form.locat.choices = [('', '-- select an option --')] + city_list

    if request.method == 'POST':
        page = int(request.form.get('page', 0))
        items_per_page = 10
        locat = request.form.get('locat', '')
        category = request.form.get('category', '')
        tut_great = request.form.get('tut_great', '')
        tut_less = request.form.get('tut_less', '')
        enroll_great = request.form.get('enroll_great', '')
        enroll_less = request.form.get('enroll_less', '')
        unobj = SQLTable('Universities')
        max_tut = unobj.select_max('tuition')
        max_stud_num = unobj.select_max('num_students')
        max_tut = max_tut + 100
        max_stud_num = max_stud_num + 100
        if not tut_great:
            tut_great = '0'
        if not tut_less:
            tut_less = max_tut
        if not enroll_great:
            enroll_great = '0'
        if not enroll_less:
            enroll_less = max_stud_num
        if locat or category or tut_less or tut_great or enroll_less or enroll_great:
            queryobj = SQLMultiTable()
            cur = queryobj.multi_join_search(locat, category, tut_great, tut_less, enroll_great, enroll_less,items_per_page,items_per_page*page)
        page_count = math.ceil(queryobj.multi_join_search_count(locat,category,tut_great,tut_less,enroll_great,enroll_less) / items_per_page) -1
        sortparams = {'sortby': 'City', 'sortdir': 'asc'}
        if page == 0:
            page = 1
        if page_count == 0:
            page_count = 1
        if page_count == -1:
            return render_template('search_no_result.html')
        resp = make_response(
            render_template('search_results.html',cur = cur, page=page, page_count=page_count, locat=locat, category=category,
                            tut_great=tut_great,tut_less=tut_less, enroll_great=enroll_great, enroll_less=enroll_less, sortparams=sortparams))

        return resp

    elif request.method == 'POST' and not form.validate():
        try:
            request.form.get('tut_less')
        except IntegerValidationError:
            flash("Provide integers :")
        return redirect("/search")
    else:
        return render_template('search.html', form=form)


if __name__ == '__main__':
    pass