from uni_app.university_app.db import SQLiteDB
import os
from uni_app.university_app import app
import sqlite3

DATABASE = app.config['DATABASE']

class SQLMultiTable:
    def __init__(self):
        self.db = SQLiteDB(DATABASE)

    def multi_join_search(self, locat:str, categ:str, tut_great:str, tut_less:str, enroll_great:str, enroll_less:str, limit:int, offset:int) -> tuple:
        sql_where = 'TRUE'
        values = {}
        sql_where = ('upper(ct.name) LIKE :locat '
                     ' AND cg.name LIKE :categ'
                     ' AND un.tuition  > :tut_great'
                     ' AND un.tuition  < :tut_less'
                     ' AND un.num_students  > :enroll_great'
                     ' AND un.num_students  < :enroll_less'
                     )
        values['locat'] = f'%{locat.upper()}%'
        values['categ'] = f'%{categ}%'
        values['tut_great'] = tut_great
        values['tut_less'] = tut_less
        values['enroll_great'] = enroll_great
        values['enroll_less'] = enroll_less
        sql_query= ( f" SELECT  un.name AS name, un.num_students AS enrollment, un.tuition AS tuition, "
            f" ct.name AS city, st.name AS state, rnk.id AS rank, un.id AS id "
            f" FROM Universities un join Cities ct ON un.city_id = ct.id JOIN "
            f" States st ON st.id = ct.state_id JOIN Programs pr ON pr.uni_id = un.id "
            f" JOIN Categories cg ON pr.category_id = cg.id LEFT JOIN UniversityRankings rnk ON rnk.uni_id=un.id WHERE {sql_where}"
            f"  GROUP BY un.name ORDER BY un.name LIMIT :limit OFFSET :offset"
         )
        values.update({'limit': limit,
                       'offset': offset})
        cur = self.db.run(sql_query, values)
        return cur

    def multi_join_search_count(self, locat:str, categ:str, tut_great:str, tut_less:str, enroll_great:str, enroll_less:str) -> int:
        sql_where = 'TRUE'
        values = {}
        sql_where = ('upper(ct.name) LIKE :locat '
                     ' AND cg.name LIKE :categ'
                     ' AND un.tuition  > :tut_great'
                     ' AND un.tuition  < :tut_less'
                     ' AND un.num_students  > :enroll_great'
                     ' AND un.num_students  < :enroll_less'
                     )
        values['locat'] = f'%{locat.upper()}%'
        values['categ'] = f'%{categ}%'
        values['tut_great'] = tut_great
        values['tut_less'] = tut_less
        values['enroll_great'] = enroll_great
        values['enroll_less'] = enroll_less
        cur =self.db.run(
            f" SELECT COUNT(DISTINCT(un.name)) FROM "
            f"Universities un join Cities ct ON un.city_id = ct.id JOIN "
            f"States st ON st.id = ct.state_id JOIN Programs pr ON pr.uni_id = un.id "
            f"JOIN Categories cg ON pr.category_id = cg.id LEFT JOIN  UniversityRankings rnk ON rnk.uni_id=un.id  WHERE {sql_where} ",values
            )
        return cur.fetchone()[0]

    def simple_join_search(self, name:str, limit:int, offset:int):
        sql_where = 'TRUE'
        values = {}
        sql_where = ('upper(un.name) LIKE :name ')
        values['name'] = f'%{name.upper()}%'
        sql_query= (
            f" SELECT un.name AS name, ct.name AS city, st.name AS state, rnk.id AS rank"
            f" FROM Universities un join Cities ct ON un.city_id = ct.id JOIN "
            f"States st ON st.id = ct.state_id "
            f"JOIN UniversityRankings rnk ON rnk.uni_id=un.id WHERE {sql_where}"
            ' ORDER BY un.name LIMIT :limit OFFSET :offset'
         )
        values.update({'limit': limit,
                       'offset': offset})
        cur = self.db.run(sql_query, values)
        return cur

    def simple_join_search_count(self, name:str) -> int:
        sql_where = 'TRUE'
        values = {}
        sql_where = ('upper(un.name) LIKE :name ')
        values['name'] = f'%{name.upper()}%'
        cur =self.db.run(
            f" SELECT COUNT(*) FROM "
            f"Universities un join Cities ct ON un.city_id = ct.id JOIN "
            f"States st ON st.id = ct.state_id "
            f"JOIN UniversityRankings rnk ON rnk.uni_id=un.id  WHERE {sql_where}",values
            )
        return cur.fetchone()[0]


if __name__ == '__main__':
    obj = SQLMultiTable()
    # cur = obj.multi_join_search('','','0','1000000000','0','10000000',10,100)
    # print(cur)
    cur = obj.multi_join_search('alfred','','15','1000000000000','3','199999990000',1000000,0)
    for i in cur:
        print(i)
    # cur = obj.multi_join_search_count('Creig', '', '', '15', '100000', '3', '1999999')
    # print(cur)


